define([
    'backbone', 'text!template/home.html'
], function (Backbone, HomeTemplate) {
    'use strict';
    return Backbone.View.extend({
        initialize: function (opts) {
            var self = this;
            this.opts = opts;
            this.model = new (Backbone.Model.extend({
                url: window.app.apiHost + "/api/me?user=" + window.app.userId
            }));
            this.model.fetch().done(function () {
                window.app.userId = self.model.get('id') || 1;
                self.render();
            });
        },
        render: function () {
            this.$el.html(_.template(HomeTemplate)({data: this.model.toJSON()}));
        }
    });
});