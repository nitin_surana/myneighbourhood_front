define([
    'jquery',
    'underscore',
    'backbone', 'text!template/carpool.html'
], function ($, _, Backbone, CarpoolTemplate) {
    'use strict';
    return Backbone.View.extend({
        initialize: function (opts) {
            var self = this;
            this.opts = opts;
            this.render();
            this.fetchCarpoolAvailable();
        },
        events: {
            'click .join-request': "join",
            'show.bs.tab': 'fetchData',
            'click .gift': 'showGiftDialog',
            'change .select-gift': 'showGiftForm',
            'click .save-gift': 'saveGift',
            'click .create-carpool': 'showCarpoolCreateModal',
            'click .save-carpool': 'saveCarpool'
        },
        showCarpoolCreateModal: function () {
            var $modal = this.$(".carpool-create-modal");
            $modal.modal();
        },
        saveCarpool: function () {
            var $modal = this.$('.carpool-create-modal')
            var $form = $modal.find('form');
            var map = $form.serializeArray();
            var dataObj = _.reduce(map, function (p, o) {
                p[o.name] = o.value;
                return p;
            }, {owner: window.app.userId});
            $.ajax({
                method: 'post',
                url: window.app.apiHost + "/api/tempcarpool",
                data: dataObj,
                context: this
            }).done(function (response) {
                if (response.status === 'success') {
                    alert(response.message || 'Carpool created successfully!');
                    $modal.modal('hide');
                }
                this.fetchCarpoolAvailable();
            });
        },
        saveGift: function () {
            var $giftForm = this.$('.gift-modal form');
            var map = $giftForm.serializeArray();
            var dataObj = _.reduce(map, function (p, o) {
                p[o.name] = o.value;
                return p;
            }, {});
            $.ajax({
                method: 'post',
                url: window.app.apiHost + "/api/gift",
                data: {
                    type: $giftForm.find('select').val(),
                    data: JSON.stringify(dataObj)
                },
                context: this
            }).done(function (response) {
                if (response.status === 'success') {
                    alert(response.message);
                    this.$(".gift-modal").modal('hide');
                }
            });
        },
        showGiftForm: function (e) {
            var giftForm = this.$(".select-gift").val();
            var $formInner = this.$(".form-inner");
            $formInner.empty();
            switch (giftForm) {
                case 'fuel-credits':
                    $formInner.append(this.$(".fuel-credits-form").html());
                    break;
                case 'gift-card':
                    $formInner.append(this.$(".gift-card-form").html());
                    break;
                default:
                    break;
            }
        },
        showGiftDialog: function (e) {
            this.$(".gift-modal").modal();
        },
        fetchData: function (e) {
            var $target = $(e.target);
            var href = $target.attr('href').replace('#', '');
            switch (href) {
                case 'carpool-requests':
                    this.fetchCarpoolRequests();
                    break;
                case 'carpool-available':
                    this.fetchCarpoolAvailable();
                    break;
                case 'carpool-my':
                    this.fetchMyCarpools()
                    break;
            }
        },
        fetchMyCarpools: function () {
            var self = this;
            var response = {
                "results":[
                    {
                        "id":1,
                        "status":"notstarted",
                        "where":"McClinctok Ave",
                        "when":"2016-03-11",
                        "owner":{
                            "id":2,
                            "first_name":"Kartik",
                            "last_name":"Desai"
                        }
                    },
                    {
                        "id":2,
                        "status":"completed",
                        "where":"1300 w Figueora Street",
                        "when":"2016-03-04",
                        "owner":{
                            "id":4,
                            "first_name":"Tampong",
                            "last_name":"Chawannakul"
                        }
                    }
                ]
            };
            // $.ajax({
            //     url: window.app.apiHost + "/api/mycarpools",
            //     context: this
            // }).done(function (response) {
                this.requestCollection = new Backbone.Collection(response.results);
                var $tbody = self.$("#carpool-my tbody");
                $tbody.empty();
                _.each(this.requestCollection.models, function (model) {
                    var $tr = $("<tr>" +
                        "<td>" + model.get('owner').first_name + "</td>" +
                        "<td>" + model.get('where') + "</td>" +
                        "<td>" + model.get('when') + "</td>" +
                        "<td><span class='label " + (model.get('status') === 'completed' ? 'label-primary' : 'label-info') + "'>" + model.get('status') + "</span>" +
                        (model.get('status') === 'completed' ? " <a href='javascript:void(0)' class='gift pull-right'>Gift</a>" : "") +
                        "</td>" +
                        "</tr>");
                    $tbody.append($tr);
                });
            // });
        },
        fetchCarpoolAvailable: function () {
            var self = this;
            $.ajax({
                url: window.app.apiHost + "/api/tempcarpool",
                data: {
                    user: window.app.userId
                },
                context: this
            }).done(function (response) {
                this.requestCollection = new Backbone.Collection(response.results);
                var $tbody = self.$("#carpool-available tbody");
                $tbody.empty();
                _.each(this.requestCollection.models, function (model) {
                    var $tr = $("<tr>" +
                        "<td>" + (model.get('owner') ? model.get('owner').first_name : '') + "</td>" +
                        "<td>" + model.get('where') + "</td>" +
                        "<td>" + model.get('when') + "</td>" +
                        "<td>" +
                        (model.get('status') === 'None' ? "<button class='btn btn-sm btn-primary join-request' data-owner-id='" + model.get('owner').id + "' data-carpool-id='" + model.get('id') + "'>" +
                        "<i class='glyphicon glyphicon-hourglass'></i> Request to join</button>" : "<span class='label label-info'>" + model.get('status') + "</span>") +
                        "</td>" +
                        "</tr>");
                    $tbody.append($tr);
                });
            });
        },
        fetchCarpoolRequests: function (e) {
            var self = this;
            $.ajax({
                url: window.app.apiHost + "/api/carpool-request?id=" + window.app.userId,
                context: this
            }).done(function (response) {
                this.requestCollection = new Backbone.Collection(response.results);
                var $tbody = self.$("#carpool-requests tbody");
                $tbody.empty();
                _.each(this.requestCollection.models, function (model) {
                    var $tr = $("<tr>" +
                        "<td>" + model.get('owner').first_name + "</td>" +
                        "<td>" + model.get('where') + "</td>" +
                        "<td>" + model.get('when') + "</td>" +
                        "<td><span class='label label-info'>" + model.get('status') + "</span></td>" +
                        "</tr>");
                    $tbody.append($tr);
                });
            });
        },
        join: function (e) {
            var $target = $(e.currentTarget);
            var carpoolId = $target.attr('data-carpool-id');
            var ownerId = $target.attr('data-owner-id');
            $.ajax({
                url: window.app.apiHost + "/api/carpool-request",
                data: {
                    id_carpool: carpoolId,
                    id_user: window.app.userId,
                    id_owner: ownerId,
                    status: 'join'
                },
                method: 'post',
                context: this
            }).done(function (response) {
                if (response.status === 'success') {
                    alert('Request sent successfully!');
                    this.$("ul.nav-tabs li:eq(2) a").tab('show');       //show the requests tab
                }
            });
        },
        render: function () {
            this.$el.html(_.template(CarpoolTemplate)());
        }
    });
});