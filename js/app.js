define(['backbone', 'underscore', 'router'], function (Backbone, _, Router) {
    'use strict';

    window.app = {};
    // window.app.apiHost = "https://demo9132901.mockable.io";
    window.app.apiHost = "https://myneighbourhood.herokuapp.com";
    window.app.userId = 1;

    new Router();
    Backbone.history.start();
});