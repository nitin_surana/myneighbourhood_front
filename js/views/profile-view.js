define([
    'jquery',
    'underscore',
    'backbone',
    'text!template/profile.html',
    'select2'
], function ($, _, Backbone, ProfileTemplate) {
    'use strict';
    return Backbone.View.extend({
        initialize: function (opts) {
            var self = this;
            this.opts = opts;
            this.model = new (Backbone.Model.extend({
                url: window.app.apiHost + '/api/profile?user=' + window.app.userId
            }));
            this.model.fetch().done(function () {
                self.render();
            });
        },
        events: {
            "submit form": "save"
        },
        render: function () {
            this.$el.html(_.template(ProfileTemplate)({data: this.model.toJSON()}));
        },
        save: function (e) {
            e.preventDefault();
            var data = this.$("form").serialize();
            $.ajax({
                url: window.app.apiHost + "/api/profile",
                data: data,
                method: 'post'
            }).done(function (response) {
                if (response.status == 'success') {
                    alert('Profile updated successfully!');
                } else {
                    alert(response);
                }
            });
        }
    });
});