define([
    'jquery',
    'underscore',
    'backbone',
    'text!template/search.html',
    'text!template/search-result.html',
    'select2'
], function ($, _, Backbone, SearchTemplate, SearchResultTemplate) {
    'use strict';
    return Backbone.View.extend({
        initialize: function (opts) {
            this.opts = opts;
            this.render();
        },
        events: {
            "keypress #q": "search",
            "click .join": "showJoinDialog",
            "click .request": "join"
        },
        showJoinDialog: function (e) {
            this.buildingId = $(e.currentTarget).attr('data-building-id');
            this.$(".modal").modal();
        },
        join: function (e) {
            var buildingId = this.buildingId;
            var notes = this.$("#notes").val();
            $.ajax({
                url: window.app.apiHost + '/api/join',
                method: 'post',
                data: {
                    "building": buildingId,
                    "notes": notes,
                    "user": window.app.userId,
                    "status": "approved"
                },
                context: this
            }).done(function (response) {
                if (response.status === 'success') {
                    alert("Join request sent successfully!");
                }
            }).always(function () {
                this.$(".modal").modal('hide');
            });
        },
        render: function () {
            this.$el.html(SearchTemplate);
        },
        search: function (e) {
            if (e.keyCode == 13) {
                var q = this.$("#q").val();
                $.ajax({
                    url: window.app.apiHost + "/api/search/" + q,
                    context: this
                }).done(function (response) {
                    if (response.status === 'success') {
                        this.searchCollection = new Backbone.Collection(response.results);
                        this.renderSearchResults();
                    }
                });
            }
        },
        renderSearchResults: function () {
            var self = this;
            var $results = this.$("ul.results");
            $results.empty();
            _.each(this.searchCollection.models, function (model) {
                var $li = _.template(SearchResultTemplate)({data: model.toJSON()});
                $results.append($li);
            });
        }
    });
});