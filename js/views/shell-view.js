define([
    'backbone', 'text!template/shell.html'
], function (Backbone, ShellTemplate) {
    'use strict';
    return Backbone.View.extend({
        template: _.template(ShellTemplate),
        initialize: function (opts) {
            this.opts = opts;
            this.render();
        },
        render: function () {
            this.$el.html(this.template);
        },
        selectMenu: function (item) {
            var $navbar = this.$(".navbar ul.nav");
            var $menu = $navbar.find("li[data-menu='" + item + "']");
            if ($menu.length) {
                $navbar.find('li').removeClass('active');
            }
            $menu.addClass('active');
        }
    });
});