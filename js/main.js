// Set the require.js configuration for your application.
require.config({
    waitSeconds: 120,
    shim: {
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        'bootstrap': {
            deps: [
                'jquery'
            ]
        }
    },
    paths: {
        jquery: 'lib/jquery',
        underscore: 'lib/underscore',
        backbone: 'lib/backbone',
        text: 'lib/text',
        'bootstrap': 'lib/bootstrap',
        'select2': 'lib/select2',
        'datatables': '//cdn.datatables.net/v/bs/jq-2.2.3/dt-1.10.12/r-2.1.0/datatables.min.js',
        'blockUI': 'lib/jquery.blockUI'
    }
});

requirejs.onError = function (err) {
    console.error(err.stack);
};

// Load our app module and pass it to our definition function
require(['app']);
