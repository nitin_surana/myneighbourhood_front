define([
    'backbone', 'text!template/notifications.html'
], function (Backbone, NotificationTemplate) {
    'use strict';
    return Backbone.View.extend({
        initialize: function (opts) {
            var self = this;
            this.opts = opts;
            this.model = new (Backbone.Model.extend({
                url: window.app.apiHost + "/api/notifications"
            }));
            // this.model.fetch().done(function () {
            //     window.app.userId = self.model.get('id') || 1;
            //     self.render();
            // });
            this.model.set({
                "status":"success",
                "results":[
                    {
                        "id":1,
                        "title":"Request approved",
                        "content":"Mark has approved your request to join his carpool",
                        "type":"simple"
                    },
                    {
                        "id":2,
                        "title":"Carpool approval request",
                        "content"   :"Sumit would like to join your carpool",
                        "type":"carpool-request"
                    },
                    {
                        "id":3,
                        "title":"Fuel credits gifted",
                        "content":"Raj sent you fuel credits for yesterday's carpool",
                        "type":"simple"
                    }
                ]
            });
            this.render();
        },
        render: function () {
            this.$el.html(_.template(NotificationTemplate)({data: this.model.toJSON()}));
        }
    });
});