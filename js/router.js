define([
    'jquery', 'underscore', 'backbone',
    'views/shell-view',
    'views/home-view',
    'views/search-view',
    'views/profile-view',
    'views/carpool-view',
    'views/notifications-view',
    'bootstrap',
    'blockUI'
], function ($, _, Backbone, ShellView, HomeView, SearchView, ProfileView, CarpoolView, NotificationsView) {
    'use strict';

    var router = Backbone.Router.extend({
        initialize: function () {
            $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
        },
        execute: function (callback, args, name) {
            this.currentView && this.currentView.remove();
            var shellView = this.shellView = this.shellView || new ShellView({
                    el: $("body")
                });
            if (callback) {
                callback.apply(this, args);
                shellView.selectMenu(name);
            }
        },
        routes: {
            '': 'home',
            'q/:q': 'home',
            'home': 'home',
            'about': 'about',
            'search': 'search',
            'register': 'register',
            'profile': 'profile',
            'carpool': 'carpool',
            'notifications': 'notifications'
        },
        home: function (q) {
            var model = new (Backbone.Model.extend({
                url: '/api/myBuilding'
            }));
            this.currentView = new HomeView({model: model, el: $("<div>").appendTo('.content')});
        },
        about: function () {
            this.currentView = new AboutView({el: $("<div>").appendTo('.content')});
        },
        search: function () {
            this.currentView = new SearchView({el: $("<div>").appendTo('.content')});
        },
        profile: function () {
            this.currentView = new ProfileView({el: $("<div>").appendTo('.content')});
        },
        carpool: function () {
            this.currentView = new CarpoolView({el: $("<div>").appendTo('.content')});
        },
        notifications: function () {
            this.currentView = new NotificationsView({el: $("<div>").appendTo('.content')});
        }
    });
    return router;
});